/*
  ==============================================================================

    OscillatorBase.h
    Created: 20 Dec 2017 11:31:48am
    Author:  Jonny Maguire

  ==============================================================================
*/
#ifndef OscillatorBase_h
#define OscillatorBase_h

#include "/Users/jonnymaguire/Documents/UWE/Year_3/SDA/Jonathan Maguire - SDA Assignment - 13009273/JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

/**
 This thw base class for the Oscillators
 */

class OscillatorBase
{
public:
  
    /** Oscillator constructor */
    OscillatorBase();
    
    

    /** Oscillator destructor */
    virtual ~OscillatorBase();
    
    
   
    /**
     Sets the frequency of the oscillator
     */
    void setFrequency (float freq);
    
    

    /**
     Gets the frequency of the oscillator
     */
    float getFrequency()                                    { return frequency; }
    
    

    /**
     Sets the amplitude of the oscillator
     */
    void setAmplitude (float amp);
    
    
   
    /**
     Gets the amplitude of the oscillator
     */
    float getAmplitude()                                    { return amplitude; }
    
    
 
    /**
     Resets the oscillator
     */
    void reset();
    
    
    
    /**
     Sets the sample rate
     */
    void setSampleRate (float sr);
    
    
   
    /**
     Returns the next sample
     */
    float nextSample();
    
    
   
    /**
     function that provides the execution of the waveshape
     */
    virtual float renderWaveShape (const float currentPhase) = 0;
    
    
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phaseInc;
    float phase;
};

#endif /* OscillatorBase_h */
