/*
  ==============================================================================

    TriangleOscillator.cpp
    Created: 20 Dec 2017 12:10:40pm
    Author:  Jonny Maguire

  ==============================================================================
*/


#include "TriangleOscillator.h"

float TriangleOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase == 0){
        out = -1.0;
    }
    else if (currentPhase > 0 && currentPhase <= M_PI){
        out = ((2 * currentPhase) / M_PI) - 1.0;
    }
    else if (currentPhase > M_PI && currentPhase <= 2 * M_PI)
    {
        out = ((2 * - currentPhase) / M_PI) + 3;
    }
    
    return out;
}
