/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a), oscillatorGUI(a)
{
    setSize (800, 500);
    
    addAndMakeVisible(oscillatorGUI);
//    addAndMakeVisible(fxandnoiseGUI);
    resized();
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    oscillatorGUI.setBoundsRelative(0, 0, 1.0, 0.8);
//    fxandnoiseGUI.setBoundsRelative(0, 0, 1.0, 0.3);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Preferences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::darkgrey, true);
        }
    }
    
}

