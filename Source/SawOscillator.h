/*
  ==============================================================================

    SawOscillator.h
    Created: 20 Dec 2017 12:10:21pm
    Author:  Jonny Maguire

  ==============================================================================
*/

#ifndef SawOscillator_h
#define SawOscillator_h

#include "OscillatorBase.h"

/**
 Class for a sawtooth wave oscillator
 */

class SawOscillator : public OscillatorBase
{
public:
    
    /**
     Function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif /* SawtoothOscillator_h */
