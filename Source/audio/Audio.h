/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "/Users/jonnymaguire/Documents/UWE/Year_3/SDA/Jonathan Maguire - SDA Assignment - 13009273/Source/OscHeader.h"
#include "maximilian.h"



class Audio :   public MidiInputCallback,
public AudioIODeviceCallback
{
public:

    /** Constructor */
    Audio();
    

    /** Destructor */
    ~Audio();
    

    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}

    /** 
     This function handles incoming MIDI messages
     */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    

    /**
     In this function, samples are sent to the output
     */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    
 
    /** */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    
    /** */
    void audioDeviceStopped() override;
    
    /**
     Retrieves sample rate of system.
     */
    double getSampleRate () const;

    /**
     This function is triggered by the OscillatorGUI combo boxes.
     @param 'int waveform' is the waveform index used in the switch case statement to select waveform.
     @param ' intoscNum' depicts the oscillator that should have its waveform changed
     @see    OscillatorGUI
     */
    void setOscillatorWaveform(int waveform, int oscNum);
    
    /**
     Sets the amplitude of the audio signal.
     */
    void setAmplitude(float newAmp);
    
    /**
     Sets the amplitude of individual Oscillators
     @see    OscillatorGUI
     */
    void setOscAmplitude(float amp[4]);
    
    /**
     Sets the cutoff frequency 0.0 - 1.0
     @see    OscillatorGUI
     */
    void setCutOffFreq(float cutoff[4]);
    
    /**
     Sets the Attack, Decay, Sustain and Release
     */
    void setADSR(float atk[4], float dcy[4], float sus[4], float rls[4]);
    void setResonance(float res[4]);
    void setChorus(float chorus);
    void setReverb(float reverb);
    void setTremRate(float tRate[4]);
    void setTremDepth(float tDepth[4]);
    void initialiseTremolo();
    void initialiseOscandAdsr();
    
    void setFrequency(float newFreq)                        {frequency = newFreq;}
    void setAttack(float newAttack)                         { attack = newAttack; }
    void setDecay(float newDecay)                           { decay = newDecay; }
    
   
    
private:
    AudioDeviceManager audioDeviceManager;
    Atomic<OscillatorBase*> oscillatorBasePtr[4];
    SinOscillator tremolo[4];
    
    //scoped pointer sorts internal logic of when stuff needs to be deleted
    // Value Trees
    
    int i, j;
   
    /** Envelope Variables */
    maxiEnv env[4], testEnv;
    float A[4], D[4], S[4], R[4];
    
    /** Filter Variables */
    maxiFilter filter[4];
    float cutoffFreq[4];
    float resonance[4];
    
    /** FX */
    Reverb reverb;
    Reverb::Parameters theReverbParameters;
    float reverbLevel;
    float tremDepth[4];
    float tremRate[4];
    
    maxiDelayline delay;
    
    maxiChorus chorus;
    float chorusLevel;
    
    /** Oscillator Variables */
    float oscAmplitudes[4];
    int numOfNotesPlaying = 0;
    float frequency;
    float sampleRate;
    float attack;
    float decay;
    
    int lastMidiNote;
    
   };


#endif /* Audio_hpp */
