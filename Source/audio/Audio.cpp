/*
 ==============================================================================
 
 Audio.cpp
 Created: 13 Nov 2014 8:14:40am
 Author:  Tom Mitchell
 
 ==============================================================================
 */

#include "Audio.h"
#include <iostream>


Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    initialiseOscandAdsr();
    initialiseTremolo();
    
    lastMidiNote = -1;
    
    theReverbParameters.dryLevel = 0.2;
    theReverbParameters.wetLevel = 0.8;
    theReverbParameters.roomSize = 0.2;
    theReverbParameters.damping = 0.2;
    
    reverb.setParameters(theReverbParameters);
    
    reverb.setSampleRate(sampleRate);
    
   
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    
    
    for(int i = 0; i < 4; i++)
        delete oscillatorBasePtr[i].get();
    
    reverb.reset();
    
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    
    if(message.isNoteOn())
    {
        for(int i = 0; i <=3; i++)
        {
        env[i].trigger = 1;
        
        }
        
    }
    
    
    if (lastMidiNote == message.getNoteNumber() && message.isNoteOff())
    {
        env[0].trigger = 0;
        
        for(int i = 0; i <=3; i++)
        {
            oscillatorBasePtr[i].get()->setFrequency(0.0);
            oscillatorBasePtr[i].get()->setAmplitude(0.0);
        }
    }
    
    if (message.isNoteOn())
    {
        lastMidiNote = message.getNoteNumber();
        
        for(int i = 0; i < 4; i++)
        {
            oscillatorBasePtr[i].get()->setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
            oscillatorBasePtr[i].get()->setAmplitude(message.getVelocity() * 0.5);
        }
    }
    
    
    
    //DEBUG Messages=====================================================================================================
    
    String midiText;
    
    if(message.isNoteOnOrOff())
    {
        midiText << " Channel " << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity" << message.getVelocity();
        
    }
    if (message.isProgramChange())
    {
        midiText << "Program " << message.getProgramChangeNumber();
        
    }
    else if (message.isPitchWheel())
    {
        midiText << "Pitch Wheel " << message.getPitchWheelValue();
    }
    //        std::cout << midiText;
    
}



void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    
    while(numSamples--)
    {
        
        float outputSample = 0.0;
        float ampEnv = 0.0;
        
        for(int i = 0; i < 4; i++)
        {
            
            float value = 0.0;
            value = oscillatorBasePtr[i].get()->nextSample();
            
            value = filter[i].lores(value, cutoffFreq[i], resonance[i]);
            
            // move to updateEnvelopes()
            ampEnv = env[i].adsr(1.0, env[i].trigger);
            if( env[i].trigger == 1 ) env[i].trigger = 0;
            
            value *= oscAmplitudes[i];
            
            outputSample += value;
            
            outputSample *= tremolo[i].nextSample() + 1;
            
        }
        
        //  outputSample = chorus.chorus(outputSample, 300.0, 0.5, 5.0, 0.5);
        //      outputSample = reverb.processMono(outputSample, sampleRate);
        
        outputSample *= ampEnv;
        outputSample *= 0.1;
        
        *outL = outputSample;
        *outR = outputSample;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
    
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
    sampleRate = device->getCurrentSampleRate();
    tremolo[4].setSampleRate(sampleRate);
    
}

double Audio::getSampleRate () const
{
    return sampleRate;
}



void Audio::audioDeviceStopped()
{
    
    
}

void Audio::setOscAmplitude (float amp[4])
{
    for(int i = 0; i <= 3; i++)
    {
        oscAmplitudes[i] =  amp[i];
        
    }
}

void Audio::setChorus(float chorus)
{
    chorusLevel = chorus;
}

void Audio::setReverb(float reverb)
{
    reverbLevel = reverb;
}

void Audio::setCutOffFreq(float cutoff[4])
{
    
    for(int i = 0; i <= 3; i++)
    {
        cutoffFreq[i] = cutoff[i];
    }
    
}

void Audio::setResonance(float res[4])
{
    for(int i = 0; i <= 3; i++)
    {
        resonance[i] = res[i];
    }
}

void Audio::setTremRate(float tRate[4])
{
    for(int i = 0; i <= 3; i++)
    {
        tremRate[i] = tRate[i];
    }
    initialiseTremolo();
}
void Audio::setTremDepth(float tDepth[4])
{
    for(int i = 0; i <= 3; i++)
    {
        tremDepth[i] = tDepth[i];
    }
    initialiseTremolo();
}

void Audio::setADSR(float atk[4], float dcy[4], float sus[4], float rls[4])
{
    for(int i = 0; i <= 3; i++)
    {
        A[i] = atk[i];
        D[i] = dcy[i];
        S[i] = sus[i];
        R[i] = rls[i];
        
        env[i].setAttack(A[i]);
        env[i].setDecay(D[i]);
        env[i].setSustain(S[i]);
        env[i].setRelease(R[i]);
    }
    
   
    
}

void Audio::setOscillatorWaveform(int waveform, int oscNum)
{
    switch (waveform)
    {
        case 0:
            oscillatorBasePtr[oscNum] = new SinOscillator();
            break;
        case 1:
            oscillatorBasePtr[oscNum] = new SquareOscillator();
            break;
        case 2:
            oscillatorBasePtr[oscNum] = new SawOscillator();
            break;
        case 3:
            oscillatorBasePtr[oscNum] = new TriangleOscillator();
            break;
        default:
            break;
    }
    
}

void Audio::initialiseTremolo()
{
    for(int i = 0; i <=3; i++)
    {
        tremolo[i].setFrequency(tremRate[i] * tremRate[i] * tremRate[i]);
        tremolo[i].setAmplitude(tremDepth[i]);
    }
}

void Audio::initialiseOscandAdsr()
{
    
    for(int i = 0; i <= 3; i++)
    {
        oscillatorBasePtr[i] = new SinOscillator;
        tremDepth[i] = 0.0;
        tremRate[i] = 0.0;
    }
    
}

