/*
  ==============================================================================

    SinOscillator.h
    Created: 20 Dec 2017 12:10:13pm
    Author:  Jonny Maguire

  ==============================================================================
*/

#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "OscillatorBase.h"

/**
 Class for a sinewave oscillator
 */

class SinOscillator : public OscillatorBase
{
public:
    
    /**
     Function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase);
};

#endif //H_SINOSCILLATOR
