/*
  ==============================================================================

    SquareOscillator.h
    Created: 20 Dec 2017 12:10:59pm
    Author:  Jonny Maguire

  ==============================================================================
*/

#ifndef SquareOscillator_h
#define SquareOscillator_h

#include "OscillatorBase.h"

/**
 Class for Squarewave Oscillator
 */

class SquareOscillator : public OscillatorBase
{
public:
    
    /**
     Function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif /* SquareOscillator_h */
