/*
  ==============================================================================

    SinOscillator.cpp
    Created: 20 Dec 2017 12:10:13pm
    Author:  Jonny Maguire

  ==============================================================================
*/

#include "SinOscillator.h"

float SinOscillator::renderWaveShape (const float currentPhase)
{
    return sin (currentPhase);
}
