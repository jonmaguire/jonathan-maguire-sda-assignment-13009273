/*
  ==============================================================================

    TriangleOscillator.h
    Created: 20 Dec 2017 12:10:40pm
    Author:  Jonny Maguire

  ==============================================================================
*/

#ifndef TriangleOscillator_h
#define TriangleOscillator_h

#include "OscillatorBase.h"

/**
 Class for triangle wave oscillator
 */

class TriangleOscillator : public OscillatorBase
{
public:
    
    /**
     Function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif /* TriangleOscillator_h */

