/*
  ==============================================================================

    SawOscillator.cpp
    Created: 20 Dec 2017 12:10:21pm
    Author:  Jonny Maguire

  ==============================================================================
*/

#include "SawOscillator.h"


float SawOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase == 0.0)
    {
        out = -1.0;
    }
    else if (currentPhase > 0.0 && currentPhase <= 2.0 * M_PI)
    {
        out = (currentPhase / M_PI) - 1.0;
    }
    
    return out * 0.4;
}
