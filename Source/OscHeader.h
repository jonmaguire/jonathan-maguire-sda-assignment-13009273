/*
  ==============================================================================

    OscHeader.h
    Created: 20 Dec 2017 11:32:17am
    Author:  Jonny Maguire

  ==============================================================================
*/

#ifndef OscillatorHeader_h
#define OscillatorHeader_h

#include "OscillatorBase.h"
#include "SinOscillator.h"
#include "TriangleOscillator.h"
#include "SawOscillator.h"
#include "SquareOscillator.h"


#endif /* OscillatorHeader_h */

