/*
  ==============================================================================

    OscillatorGUI.cpp
    Created: 20 Dec 2017 11:31:18am
    Author:  Jonny Maguire

  ==============================================================================
*/

#include "OscillatorGUI.h"


OscillatorGUI::OscillatorGUI(Audio& a) :   audio (a)

{
    i = 0;
    j = 0;
    
    initialiseComboBox();
    initialiseSliders();
    initialiseButtons();
    initialiseLabels();
    
    getLookAndFeel().setColour (Slider::thumbColourId, Colours::cyan);
    
}


OscillatorGUI::~OscillatorGUI()
{
    
}


void OscillatorGUI::resized()
{

    area = getLocalBounds();
    
    //Dividing the GUI into panels for controls to be assigned
    
    osc1ControlPanel1.setBounds(0, 0, getWidth()/8, getHeight()/5);
    osc1ControlPanel2.setBounds(100, 0, getWidth()/8, getHeight()/5);
    osc1ControlPanel3.setBounds(300, 0, getWidth()/4, getHeight()/5);
    osc1ControlPanel4top.setBounds(500, 0, getWidth()/8, getHeight()/10);
    osc1ControlPanel4bottom.setBounds(500, 50, getWidth()/8, getHeight()/10);
    osc1ControlPanel5.setBounds(200, 0, getWidth()/8, getHeight()/5);
    osc1ControlPanel6.setBounds(600, 0, getWidth()/5.7, getHeight()/5);
    osc1ControlPanel7.setBounds(740, 10, getWidth()/15, getHeight()/5);
    
    osc2ControlPanel1.setBounds(0, 100, getWidth()/8, getHeight()/5);
    osc2ControlPanel2.setBounds(100, 100, getWidth()/8, getHeight()/5);
    osc2ControlPanel3.setBounds(300, 100, getWidth()/4, getHeight()/5);
    osc2ControlPanel4top.setBounds(500, 100, getWidth()/8, getHeight()/10);
    osc2ControlPanel4bottom.setBounds(500, 150, getWidth()/8, getHeight()/10);
    osc2ControlPanel5.setBounds(200, 100, getWidth()/8, getHeight()/5);
    osc2ControlPanel6.setBounds(600, 100, getWidth()/5.7, getHeight()/5);
    osc2ControlPanel7.setBounds(740, 110, getWidth()/15, getHeight()/5);
    
    osc3ControlPanel1.setBounds(0, 200, getWidth()/8, getHeight()/5);
    osc3ControlPanel2.setBounds(100, 200, getWidth()/8, getHeight()/5);
    osc3ControlPanel3.setBounds(300, 200, getWidth()/4, getHeight()/5);
    osc3ControlPanel4top.setBounds(500, 200, getWidth()/8, getHeight()/10);
    osc3ControlPanel4bottom.setBounds(500, 250, getWidth()/8, getHeight()/10);
    osc3ControlPanel5.setBounds(200, 200, getWidth()/8, getHeight()/5);
    osc3ControlPanel6.setBounds(600, 200, getWidth()/5.7, getHeight()/5);
    osc3ControlPanel7.setBounds(740, 210, getWidth()/15, getHeight()/5);
    
    osc4ControlPanel1.setBounds(0, 300, getWidth()/8, getHeight()/5);
    osc4ControlPanel2.setBounds(100, 300, getWidth()/8, getHeight()/5);
    osc4ControlPanel3.setBounds(300, 300, getWidth()/4, getHeight()/5);
    osc4ControlPanel4top.setBounds(500, 300, getWidth()/8, getHeight()/10);
    osc4ControlPanel4bottom.setBounds(500, 350, getWidth()/8, getHeight()/10);
    osc4ControlPanel5.setBounds(200, 300, getWidth()/8, getHeight()/5);
    osc4ControlPanel6.setBounds(600, 300, getWidth()/5.7, getHeight()/5);
    osc4ControlPanel7.setBounds(740, 310, getWidth()/15, getHeight()/5);
    
 
    fxPanel[0].setBounds(500, 400, getWidth()/8, getHeight()/5);
    fxPanel[1].setBounds(600, 400, getWidth()/8, getHeight()/5);
    fxPanel[2].setBounds(700, 400, getWidth()/8, getHeight()/5);
    

    
    //============================================================================================
    
    amplitudeSlider[0].setBounds(osc1ControlPanel1);
    amplitudeSlider[1].setBounds(osc2ControlPanel1);
    amplitudeSlider[2].setBounds(osc3ControlPanel1);
    amplitudeSlider[3].setBounds(osc4ControlPanel1);
    
    cutoffSlider[0].setBounds(osc1ControlPanel2);
    cutoffSlider[1].setBounds(osc2ControlPanel2);
    cutoffSlider[2].setBounds(osc3ControlPanel2);
    cutoffSlider[3].setBounds(osc4ControlPanel2);
  
    oscillatorComboBox[0].setBounds(osc1ControlPanel3);
    oscillatorComboBox[1].setBounds(osc2ControlPanel3);
    oscillatorComboBox[2].setBounds(osc3ControlPanel3);
    oscillatorComboBox[3].setBounds(osc4ControlPanel3);
    
    tremDepth[0].setBounds(osc1ControlPanel4top);
    tremDepth[1].setBounds(osc2ControlPanel4top);
    tremDepth[2].setBounds(osc3ControlPanel4top);
    tremDepth[3].setBounds(osc4ControlPanel4top);
    
    tremSpeed[0].setBounds(osc1ControlPanel4bottom);
    tremSpeed[1].setBounds(osc2ControlPanel4bottom);
    tremSpeed[2].setBounds(osc3ControlPanel4bottom);
    tremSpeed[3].setBounds(osc4ControlPanel4bottom);
    
    resDial[0].setBounds(osc1ControlPanel5);
    resDial[1].setBounds(osc2ControlPanel5);
    resDial[2].setBounds(osc3ControlPanel5);
    resDial[3].setBounds(osc4ControlPanel5);
    

    
    
    adsrA[0].setBounds(600, 0, 20, 100);
    adsrD[0].setBounds(640, 0, 20, 100);
    adsrS[0].setBounds(680, 0, 20, 100);
    adsrR[0].setBounds(720, 0, 20, 100);
    
    adsrA[1].setBounds(600, 100, 20, 100);
    adsrD[1].setBounds(640, 100, 20, 100);
    adsrS[1].setBounds(680, 100, 20, 100);
    adsrR[1].setBounds(720, 100, 20, 100);
    
    adsrA[2].setBounds(600, 200, 20, 100);
    adsrD[2].setBounds(640, 200, 20, 100);
    adsrS[2].setBounds(680, 200, 20, 100);
    adsrR[2].setBounds(720, 200, 20, 100);

    adsrA[3].setBounds(600, 300, 20, 100);
    adsrD[3].setBounds(640, 300, 20, 100);
    adsrS[3].setBounds(680, 300, 20, 100);
    adsrR[3].setBounds(720, 300, 20, 100);

   //=========================================================================================
    
    ampLabel[0].setBounds(osc1ControlPanel1);
    ampLabel[1].setBounds(osc2ControlPanel1);
    ampLabel[2].setBounds(osc3ControlPanel1);
    ampLabel[3].setBounds(osc4ControlPanel1);
    
    cutoffLabel[0].setBounds(osc1ControlPanel2);
    cutoffLabel[1].setBounds(osc2ControlPanel2);
    cutoffLabel[2].setBounds(osc3ControlPanel2);
    cutoffLabel[3].setBounds(osc4ControlPanel2);
    
    resLabel[0].setBounds(osc1ControlPanel5);
    resLabel[1].setBounds(osc2ControlPanel5);
    resLabel[2].setBounds(osc3ControlPanel5);
    resLabel[3].setBounds(osc4ControlPanel5);

    
    tremDepthLabel[0].setBounds(osc1ControlPanel4top);
    tremDepthLabel[1].setBounds(osc2ControlPanel4top);
    tremDepthLabel[2].setBounds(osc3ControlPanel4top);
    tremDepthLabel[3].setBounds(osc4ControlPanel4top);
    
    tremSpeedLabel[0].setBounds(osc1ControlPanel4bottom);
    tremSpeedLabel[1].setBounds(osc2ControlPanel4bottom);
    tremSpeedLabel[2].setBounds(osc3ControlPanel4bottom);
    tremSpeedLabel[3].setBounds(osc4ControlPanel4bottom);
    
    adsrLabel[0].setBounds(osc1ControlPanel6);
    adsrLabel[1].setBounds(osc2ControlPanel6);
    adsrLabel[2].setBounds(osc3ControlPanel6);
    adsrLabel[3].setBounds(osc4ControlPanel6);
    
    oscLabel[0].setBounds(osc1ControlPanel7);
    oscLabel[1].setBounds(osc2ControlPanel7);
    oscLabel[2].setBounds(osc3ControlPanel7);
    oscLabel[3].setBounds(osc4ControlPanel7);
    
//    appName.setBounds(area);
//    appName.setJustificationType(Justification::centredBottom);
    
    //Justification loop to position labels within panels
    for(int i = 0; i < 4; i++)
    {
        ampLabel[i].setJustificationType(Justification::centredBottom);
        cutoffLabel[i].setJustificationType(Justification::centredBottom);
        tremDepthLabel[i].setJustificationType(Justification::centredBottom);
        tremSpeedLabel[i].setJustificationType(Justification::centredBottom);
        adsrLabel[i].setJustificationType(Justification::centredBottom);
        resLabel[i].setJustificationType(Justification::centredBottom);
        oscLabel[i].setJustificationType(Justification::centred);
    }

    
}

void OscillatorGUI::paint(Graphics& g)
{
  
    g.setColour(Colours::darkred);

    g.drawRect(fxPanel[0]);
    g.drawRect(fxPanel[1]);
    g.drawRect(fxPanel[2]);
    
//    g.drawRect(osc1ControlPanel2);
//    g.drawRect(osc1ControlPanel3);
//    g.drawRect(osc1ControlPanel4top);
//    g.drawRect(osc1ControlPanel4bottom);
//    g.drawRect(osc1ControlPanel5);
//    g.drawRect(osc1ControlPanel6);
//    g.drawRect(osc1ControlPanel7);
//    
//    g.drawRect(osc2ControlPanel1);
//    g.drawRect(osc2ControlPanel2);
//    g.drawRect(osc2ControlPanel3);
//    g.drawRect(osc2ControlPanel4top);
//    g.drawRect(osc2ControlPanel4bottom);
//    g.drawRect(osc2ControlPanel5);
//    g.drawRect(osc2ControlPanel6);
//    g.drawRect(osc2ControlPanel7);
//    
//    
//    g.drawRect(osc3ControlPanel1);
//    g.drawRect(osc3ControlPanel2);
//    g.drawRect(osc3ControlPanel3);
//    g.drawRect(osc3ControlPanel4top);
//    g.drawRect(osc3ControlPanel4bottom);
//    g.drawRect(osc3ControlPanel5);
//    g.drawRect(osc3ControlPanel6);
//    g.drawRect(osc3ControlPanel7);
//    
//    g.drawRect(osc4ControlPanel1);
//    g.drawRect(osc4ControlPanel2);
//    g.drawRect(osc4ControlPanel3);
//    g.drawRect(osc4ControlPanel4top);
//    g.drawRect(osc4ControlPanel4bottom);
//    g.drawRect(osc4ControlPanel5);
//    g.drawRect(osc4ControlPanel6);
//    g.drawRect(osc4ControlPanel7);
//    
    
}


void OscillatorGUI::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    for(int i = 0; i < 4; i++)
    if (comboBoxThatHasChanged == &oscillatorComboBox[i])
    {
        switch (oscillatorComboBox[i].getSelectedItemIndex()) {
            case 0:
                audio.setOscillatorWaveform(oscillatorComboBox[i].getSelectedItemIndex(), i);
                break;
            case 1:
                audio.setOscillatorWaveform(oscillatorComboBox[i].getSelectedItemIndex(), i);
                break;
            case 2:
                audio.setOscillatorWaveform(oscillatorComboBox[i].getSelectedItemIndex(), i);
                break;
            case 3:
                audio.setOscillatorWaveform(oscillatorComboBox[i].getSelectedItemIndex(), i);
                break;
            default:
                break;
        }
    }

}




void OscillatorGUI::sliderValueChanged (Slider* slider)
{
    
    for(int i = 0; i <= 3; i++)
    {
        oscAmplitudes[i] = amplitudeSlider[i].getValue();

        cutOffFreq[i] = cutoffSlider[i].getValue();
        
        res[i] = resDial[i].getValue();
        
        atk[i] = adsrA[i].getValue();
        dcy[i] = adsrD[i].getValue();
        sus[i] = adsrS[i].getValue();
        rls[i] = adsrR[i].getValue();
        
        tremR[i] = tremSpeed[i].getValue();
        tremD[i] = tremDepth[i].getValue();
        

    }
 

    
    
    audio.setOscAmplitude(oscAmplitudes); // this passes osc amplitudes into amp[] array - later assigned in audio cpp
    audio.setCutOffFreq(cutOffFreq); // this passes cutOffFreq into cutoff[] array - later assigned in audio cpp
    audio.setADSR(atk, dcy, sus, rls);
    audio.setResonance(res);
    audio.setTremRate(tremR);
    audio.setTremDepth(tremD);
}


void OscillatorGUI::buttonClicked (Button* button)
{

}



void OscillatorGUI::initialiseSliders()
{
    for(int i = 0; i <= 3; i++)
    {
        
        amplitudeSlider[i].setSliderStyle(juce::Slider::Rotary);
        amplitudeSlider[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        amplitudeSlider[i].setRange(0.0, 0.1);
        amplitudeSlider[i].setValue(0.05);
        amplitudeSlider[i].addListener(this);
        addAndMakeVisible(amplitudeSlider[i]);
        
        cutoffSlider[i].setSliderStyle(juce::Slider::Rotary);
        cutoffSlider[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        cutoffSlider[i].setRange(200.0, 800.0);
        cutoffSlider[i].setValue(500.0);
        cutoffSlider[i].addListener(this);
        addAndMakeVisible(cutoffSlider[i]);
    
        tremDepth[i].setSliderStyle(juce::Slider::Rotary);
        tremDepth[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        tremDepth[i].setRange(0.0, 1.0);
        tremDepth[i].setValue(0.0);
        tremDepth[i].addListener(this);
        addAndMakeVisible(tremDepth[i]);
        
        tremSpeed[i].setSliderStyle(juce::Slider::Rotary);
        tremSpeed[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        tremSpeed[i].setRange(0.3, 2.5);
        tremSpeed[i].setValue(0.3);
        tremSpeed[i].addListener(this);
        addAndMakeVisible(tremSpeed[i]);
        
        resDial[i].setSliderStyle(juce::Slider::Rotary);
        resDial[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        resDial[i].setRange(1.0, 10.0);
        resDial[i].setValue(0.5);
        resDial[i].addListener(this);
        addAndMakeVisible(resDial[i]);
        
        adsrA[i].setSliderStyle(juce::Slider::LinearVertical);
        adsrA[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        adsrA[i].setRange(50.0, 8000.0);
        adsrA[i].setValue(6000.0);
        adsrA[i].addListener(this);
        
        adsrD[i].setSliderStyle(juce::Slider::LinearVertical);
        adsrD[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        adsrD[i].setRange(50.0, 8000.0);
        adsrD[i].setValue(500.0);
        adsrD[i].addListener(this);
        
        adsrS[i].setSliderStyle(juce::Slider::LinearVertical);
        adsrS[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        adsrS[i].setRange(1.0, 2000.0);
        adsrS[i].setValue(10.0);
        adsrS[i].addListener(this);
        
        adsrR[i].setSliderStyle(juce::Slider::LinearVertical);
        adsrR[i].setTextBoxStyle(Slider::NoTextBox, true, true, true);
        adsrR[i].setRange(50.0, 8000.0);
        adsrR[i].setValue(4000.0);
        adsrR[i].addListener(this);
        
        
        addAndMakeVisible(adsrA[i]);
        addAndMakeVisible(adsrD[i]);
        addAndMakeVisible(adsrS[i]);
        addAndMakeVisible(adsrR[i]);
   
        
    }
    
 
    
}


void OscillatorGUI::initialiseComboBox()
{
    StringArray Waveforms = {"Sine", "Square", "Sawtooth", "Triangle"};
    
    for(int i = 0; i <=3 ; i++)
    {
        oscillatorComboBox[i].addItemList(Waveforms, 1);
        oscillatorComboBox[i].setColour(ComboBox::backgroundColourId, Colour(0x00ffffff));
        oscillatorComboBox[i].setColour(ComboBox::outlineColourId, Colour(0xFF34495E));
        oscillatorComboBox[i].setColour(ComboBox::textColourId, Colour(0xff000000));
        oscillatorComboBox[i].addListener(this);
        
        addAndMakeVisible(oscillatorComboBox[i]);
    }
    
    oscillatorComboBox[0].setSelectedId(1);
    oscillatorComboBox[1].setSelectedId(2);
    oscillatorComboBox[2].setSelectedId(3);
    oscillatorComboBox[3].setSelectedId(4);
}


void OscillatorGUI::initialiseButtons()
{

}

void OscillatorGUI:: initialiseLabels()
{
    for(int i = 0; i < 4; i++)
    {
        
    ampLabel[i].setText("Amplitude", dontSendNotification);
    ampLabel[i].setFont(Font("Open Sans", 9, 0));
    ampLabel[i].setColour(Label::ColourIds::textColourId, Colours::lightslategrey);
    ampLabel[i].setInterceptsMouseClicks(false, false);
    addAndMakeVisible(ampLabel[i]);
        
    cutoffLabel[i].setText("Cutoff", dontSendNotification);
    cutoffLabel[i].setFont(Font("Open Sans", 9, 0));
    cutoffLabel[i].setColour(Label::ColourIds::textColourId, Colours::lightslategrey);
    cutoffLabel[i].setInterceptsMouseClicks(false, false);
    addAndMakeVisible(cutoffLabel[i]);
    
    resLabel[i].setText("Resonance", dontSendNotification);
    resLabel[i].setFont(Font("Open Sans", 9, 0));
    resLabel[i].setColour(Label::ColourIds::textColourId, Colours::lightslategrey);
    resLabel[i].setInterceptsMouseClicks(false, false);
    addAndMakeVisible(resLabel[i]);
        
    tremDepthLabel[i].setText("Trem Depth", dontSendNotification);
    tremDepthLabel[i].setFont(Font("Open Sans", 9, 0));
    tremDepthLabel[i].setColour(Label::ColourIds::textColourId, Colours::lightslategrey);
    tremDepthLabel[i].setInterceptsMouseClicks(false, false);
    addAndMakeVisible(tremDepthLabel[i]);
    
    tremSpeedLabel[i].setText("Trem Speed", dontSendNotification);
    tremSpeedLabel[i].setFont(Font("Open Sans", 9, 0));
    tremSpeedLabel[i].setColour(Label::ColourIds::textColourId, Colours::lightslategrey);
    tremSpeedLabel[i].setInterceptsMouseClicks(false, false);
    addAndMakeVisible(tremSpeedLabel[i]);
    
    adsrLabel[i].setText("ADSR", dontSendNotification);
    adsrLabel[i].setFont(Font("Open Sans", 10, 0));
    adsrLabel[i].setColour(Label::ColourIds::textColourId, Colours::lightslategrey);
    adsrLabel[i].setInterceptsMouseClicks(false, false);
    addAndMakeVisible(adsrLabel[i]);
        
    oscLabel[i].setFont(Font("Open Sans", 18, 0));
    oscLabel[i].setColour (Label::ColourIds::textColourId, Colours::lightslategrey);
    oscLabel[i].setInterceptsMouseClicks(false, false);
    addAndMakeVisible (oscLabel[i]);
        
        
    }
    
//    appName.setText("JS1 Synthesiser", dontSendNotification);
//    appName.setFont(Font("Open Sans", 30, 0));
//    appName.setColour(Label::ColourIds::textColourId, Colours::lightslategrey);
//    appName.setInterceptsMouseClicks(false, false);
//    addAndMakeVisible(appName);
    
    oscLabel[0].setText("O\nS\nC\n1", dontSendNotification);
    oscLabel[1].setText("O\nS\nC\n2", dontSendNotification);
    oscLabel[2].setText("O\nS\nC\n3", dontSendNotification);
    oscLabel[3].setText("O\nS\nC\n4", dontSendNotification);

}
