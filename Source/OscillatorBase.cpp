/*
  ==============================================================================

    OscillatorBase.cpp
    Created: 20 Dec 2017 11:31:48am
    Author:  Jonny Maguire

  ==============================================================================
*/

#include "OscillatorBase.h"

OscillatorBase::OscillatorBase()
{
    reset();
}




OscillatorBase::~OscillatorBase()
{
    
}




void OscillatorBase::setAmplitude (float amp)
{
    amplitude = amp;
}




void OscillatorBase::setFrequency (float freq)
{
    for(int i = 0; i < 4; i++)
    {
    frequency = freq;
    phaseInc = (2 * M_PI * frequency) / sampleRate ;
    }

}




void OscillatorBase::reset()
{
    phase = 0.f;
    sampleRate = 44100.f; //
    setFrequency (0.f);
    setAmplitude (0.f);
}


void OscillatorBase::setSampleRate (float sr)
{
    sampleRate = sr; // currently not working, using sampleRate in reset.
    setFrequency (frequency); //just to update the phaseInc
}



float OscillatorBase::nextSample()
{
    float out = renderWaveShape(phase ) * amplitude;
    phase += phaseInc ;
    if(phase  > (2.f * M_PI))
        phase -= (2.f * M_PI);
    
    return out;
}
