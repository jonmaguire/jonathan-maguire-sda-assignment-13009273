/*
  ==============================================================================

    SquareOscillator.cpp
    Created: 20 Dec 2017 12:10:59pm
    Author:  Jonny Maguire

  ==============================================================================
*/

#include "SquareOscillator.h"

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase <= M_PI)
    {
        out = 1.0;
    }
    else if(currentPhase >= M_PI)
    {
        out = -1.0;
    }
    else {
        out = 0.0;
    }
    
    return out * 0.4;
}
