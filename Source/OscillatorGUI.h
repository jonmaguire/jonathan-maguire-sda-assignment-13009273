/*
  ==============================================================================

    OscillatorGUI.h
    Created: 20 Dec 2017 11:31:18am
    Author:  Jonny Maguire

  ==============================================================================
*/

#ifndef OscillatorGUI_hpp
#define OscillatorGUI_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "/Users/jonnymaguire/Documents/UWE/Year_3/SDA/Jonathan Maguire - SDA Assignment - 13009273/Source/audio/Audio.h"
#include "OscHeader.h"
#include "../Source/audio/Audio.h"


/**
 This class makes up the entire UI featuring all controls
 */

class OscillatorGUI :   public Component,
                        public Button::Listener,
                        public Slider::Listener,
                        public ComboBox::Listener
{
public:

    /** Constructor */
    OscillatorGUI(Audio& a);
    
    

    /** Destructor */
    ~OscillatorGUI();
    
    

    /**
     Initialises the four comboboxes called in the constructor.
     */
    void initialiseComboBox();
    
    

    /**
     Initialises the sliders
     */
    void initialiseSliders();
    
    

    /**
     Initialises the buttons
     */
    void initialiseButtons();
    
    
 
    /**
     Initialises the labels
     */
    void initialiseLabels();
    
    
  
    /**
     Resizes components within the class
     */
    void resized() override;
    

    
    /**
     Paints the control panel layout for the GUI
     */
    void paint(Graphics& g) override;
    
    

    /**
     Dropdown to select waveform
     @see    Audio
     */
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    

    /**
     Sliders set amplitude of each oscillator, tremolo speed and depth, ADSR and Cutoff
     @see    Audio
     */
    void sliderValueChanged (Slider* slider) override;
    
    

    /**
     This button...
    
     */
    void buttonClicked (Button* button) override;
    
    
private:
    
    int i, j;
    float oscAmplitudes[4], cutOffFreq[4], atk[4], dcy[4], sus[4], rls[4], res[4], tremR[4], tremD[4];
    
    Slider amplitudeSlider[4], cutoffSlider[4], tremDepth[4], tremSpeed[4], resDial[4], adsrA[4], adsrD[4], adsrS[4], adsrR[4];
    
    ComboBox oscillatorComboBox[4];
    
    Label appName, ampLabel[4], cutoffLabel[4], resLabel[4], tremDepthLabel[4], tremSpeedLabel[4], adsrLabel[4], oscLabel[4];
    
    Rectangle<int> area, fxPanel[3];
    
    Rectangle<int> osc1ControlPanel1, osc1ControlPanel2, osc1ControlPanel3, osc1ControlPanel4top, osc1ControlPanel4bottom, osc1ControlPanel5, osc1ControlPanel6, osc1ControlPanel7;
    
    Rectangle<int> osc2ControlPanel1, osc2ControlPanel2, osc2ControlPanel3, osc2ControlPanel4top, osc2ControlPanel4bottom, osc2ControlPanel5, osc2ControlPanel6, osc2ControlPanel7;
    
    Rectangle<int> osc3ControlPanel1, osc3ControlPanel2, osc3ControlPanel3, osc3ControlPanel4top, osc3ControlPanel4bottom, osc3ControlPanel5, osc3ControlPanel6, osc3ControlPanel7;
    
    Rectangle<int> osc4ControlPanel1, osc4ControlPanel2, osc4ControlPanel3, osc4ControlPanel4top, osc4ControlPanel4bottom, osc4ControlPanel5, osc4ControlPanel6, osc4ControlPanel7;
    
    
    
    Audio& audio;
};

#endif /* OscillatorGUI_hpp */
